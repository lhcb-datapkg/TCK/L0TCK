//-------------------------------------------------------
// Created by PVSS / L0DU  : 2018-09-19 16:11:15
//-------------------------------------------------------

//------------------------- TCK = 0x1803 / Recipe name : PHYSICS_Sep2018_TCK1803_0x1803
ToolSvc.L0DUConfig.registerTCK += {"0x1803"};
ToolSvc.L0DUConfig.TCK_0x1803.Name = "PHYSICS_Sep2018_TCK1803_0x1803";
ToolSvc.L0DUConfig.TCK_0x1803.Description ="TCK 0x1803: Top of ramp TCK with tighter ElectronLoose and without Herschel";

ToolSvc.L0DUConfig.TCK_0x1803.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x1803.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x1803.Conditions = {
{ "name=[Electron(Et)>20]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"                        },
{ "name=[Electron(Et)>37]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[37]"                        },
{ "name=[Electron(Et)>50]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"                        },
{ "name=[Electron(Et)>99]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[99]"                        },
{ "name=[Electron(Et)>254]"   , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"                       },
{ "name=[Photon(Et)>20]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"                        },
{ "name=[Photon(Et)>50]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"                        },
{ "name=[Photon(Et)>123]"     , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[123]"                       },
{ "name=[Photon(Et)>254]"     , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"  , "reported=[FALSE]" },
{ "name=[Hadron(Et)>10]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"                        },
{ "name=[Hadron(Et)>25]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[25]"                        },
{ "name=[Hadron(Et)>158]"     , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[158]"                       },
{ "name=[Hadron(Et)>254]"     , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"  , "reported=[FALSE]" },
{ "name=[SumEt>1458]"         , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"                      },
{ "name=[SumEt<135]"          , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[135]"                       },
{ "name=[SumEt>208]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"                       },
{ "name=[SumEtPrev<250]"      , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[250]"  , "bx=[-1]"          },
{ "name=[SumEtPrev<1000]"     , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[1000]" , "bx=[-1]"          },
{ "name=[Spd_Had(Mult)<450]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"                       },
{ "name=[Spd_DiMu(Mult)<900]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"                       },
{ "name=[Spd(Mult)<20]"       , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"                        },
{ "name=[Spd(Mult)<10]"       , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10]"                        },
{ "name=[Spd(Mult)<15]"       , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[15]"                        },
{ "name=[Spd(Mult)>2]"        , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[PU(Mult)<18]"        , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[18]"                        },
{ "name=[PU(Mult)<2]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"                         },
{ "name=[PU(Mult)>9]"         , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"                         },
{ "name=[Muon1(Pt)>2]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[Muon1(Pt)>8]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"                         },
{ "name=[Muon1(Pt)>18]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[18]"                        },
{ "name=[Muon1(Pt)>35]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[35]"                        },
{ "name=[Muon1(Pt)>120]"      , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"                       },
{ "name=[Muon2(Pt)>2]"        , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[Muon12(Pt)>1296]"    , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[1296]"                      }
};

ToolSvc.L0DUConfig.TCK_0x1803.Channels ={
{ "name=[JetEl]"            , "rate==[100]" , "conditions= [Electron(Et)>254]   && [Hadron(Et)>254]    && [SumEt>1458]                       " , "MASK=[001]" },
{ "name=[JetPh]"            , "rate==[100]" , "conditions= [Photon(Et)>254]     && [Hadron(Et)>254]    && [SumEt>1458]                       " , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]" , "conditions= [Electron(Et)>99]    && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                " , "MASK=[001]" },
{ "name=[ElectronLoose]"    , "rate==[0.7]" , "conditions= [Electron(Et)>37]    && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                " , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]" , "conditions= [Photon(Et)>123]     && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                " , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]" , "conditions= [Hadron(Et)>158]     && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                " , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]" , "conditions= [SumEtPrev<1000]     && [Spd_Had(Mult)<450] && [Muon1(Pt)>35]                     " , "MASK=[001]" },
{ "name=[MuonLoose]"        , "rate==[0.2]" , "conditions= [SumEtPrev<1000]     && [Spd_Had(Mult)<450] && [Muon1(Pt)>18]                     " , "MASK=[001]" },
{ "name=[MuonEW]"           , "rate==[100]" , "conditions= [Muon1(Pt)>120]                                                                   " , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900] && [Muon12(Pt)>1296]                                         " , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]" , "conditions= [SumEt>208]          && [PU(Mult)<18]                                             " , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]" , "conditions= [SumEt<135]          && [PU(Mult)>9]                                              " , "MASK=[100]" },
{ "name=[CALO]"             , "rate==[100]" , "conditions= [Hadron(Et)>10]      && [Spd(Mult)>2]                                             " , "MASK=[000]" },
{ "name=[DiHadron,lowMult]" , "rate==[80]"  , "conditions= [Hadron(Et)>25]      && [SumEtPrev<250]     && [Spd(Mult)<10]      && [PU(Mult)<2]" , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]" , "conditions= [Spd(Mult)<20]       && [Muon1(Pt)>8]                                             " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]" , "conditions= [Spd(Mult)<20]       && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                      " , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]" , "conditions= [Electron(Et)>50]    && [Spd(Mult)<15]                                            " , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]" , "conditions= [Photon(Et)>50]      && [Spd(Mult)<15]                                            " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[50]"  , "conditions= [Electron(Et)>20]    && [Photon(Et)>20]     && [Spd(Mult)<15]                     " , "MASK=[001]" }
};

//------------------------- TCK = 0x18A3 / Recipe name : PHYSICS_Sep2018_TCK18A3_0x18A3
ToolSvc.L0DUConfig.registerTCK += {"0x18A3"};
ToolSvc.L0DUConfig.TCK_0x18A3.Name = "PHYSICS_Sep2018_TCK18A3_0x18A3";
ToolSvc.L0DUConfig.TCK_0x18A3.Description ="TCK 0x18A3: Top of ramp TCK with tighter ElectronLoose and Herschel line prescaled to 0.2.";

ToolSvc.L0DUConfig.TCK_0x18A3.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x18A3.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x18A3.Conditions = {
{ "name=[Electron(Et)>20]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"                        },
{ "name=[Electron(Et)>37]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[37]"                        },
{ "name=[Electron(Et)>50]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"                        },
{ "name=[Electron(Et)>99]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[99]"                        },
{ "name=[Electron(Et)>254]"   , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"                       },
{ "name=[Photon(Et)>20]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"                        },
{ "name=[Photon(Et)>50]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"   , "reported=[FALSE]" },
{ "name=[Photon(Et)>123]"     , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[123]"                       },
{ "name=[Photon(Et)>254]"     , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"  , "reported=[FALSE]" },
{ "name=[HRC(B)<1]"           , "data=[GlobalPi0(Et)]"      , "comparator=[<]" , "threshold=[1]"                         },
{ "name=[HRC(F)<1]"           , "data=[LocalPi0(Et)]"       , "comparator=[<]" , "threshold=[1]"                         },
{ "name=[Hadron(Et)>10]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"                        },
{ "name=[Hadron(Et)>15]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[15]"                        },
{ "name=[Hadron(Et)>158]"     , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[158]"                       },
{ "name=[Hadron(Et)>254]"     , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"  , "reported=[FALSE]" },
{ "name=[SumEt>1458]"         , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"                      },
{ "name=[SumEt<135]"          , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[135]"                       },
{ "name=[SumEt>208]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"                       },
{ "name=[SumEtPrev<200]"      , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[200]"  , "bx=[-1]"          },
{ "name=[SumEtPrev<1000]"     , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[1000]" , "bx=[-1]"          },
{ "name=[Spd_Had(Mult)<450]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"                       },
{ "name=[Spd_DiMu(Mult)<900]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"                       },
{ "name=[Spd(Mult)<20]"       , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"                        },
{ "name=[Spd(Mult)<15]"       , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[15]"                        },
{ "name=[Spd(Mult)>2]"        , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[PU(Mult)<18]"        , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[18]"                        },
{ "name=[PU(Mult)<2]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"                         },
{ "name=[PU(Mult)>9]"         , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"                         },
{ "name=[Muon1(Pt)>2]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[Muon1(Pt)>8]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"                         },
{ "name=[Muon1(Pt)>18]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[18]"                        },
{ "name=[Muon1(Pt)>35]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[35]"                        },
{ "name=[Muon1(Pt)>120]"      , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"                       },
{ "name=[Muon2(Pt)>2]"        , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[Muon12(Pt)>1296]"    , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[1296]"                      }
};

ToolSvc.L0DUConfig.TCK_0x18A3.Channels ={
{ "name=[JetEl]"               , "rate==[100]" , "conditions= [Electron(Et)>254]   && [Hadron(Et)>254]    && [SumEt>1458]                                                            " , "MASK=[001]" },
{ "name=[JetPh]"               , "rate==[100]" , "conditions= [Photon(Et)>254]     && [Hadron(Et)>254]    && [SumEt>1458]                                                            " , "MASK=[001]" },
{ "name=[Electron]"            , "rate==[100]" , "conditions= [Electron(Et)>99]    && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[ElectronLoose]"       , "rate==[0.4]" , "conditions= [Electron(Et)>37]    && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[Photon]"              , "rate==[100]" , "conditions= [Photon(Et)>123]     && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[Hadron]"              , "rate==[100]" , "conditions= [Hadron(Et)>158]     && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[Muon]"                , "rate==[100]" , "conditions= [SumEtPrev<1000]     && [Spd_Had(Mult)<450] && [Muon1(Pt)>35]                                                          " , "MASK=[001]" },
{ "name=[MuonLoose]"           , "rate==[0.2]" , "conditions= [SumEtPrev<1000]     && [Spd_Had(Mult)<450] && [Muon1(Pt)>18]                                                          " , "MASK=[001]" },
{ "name=[MuonEW]"              , "rate==[100]" , "conditions= [Muon1(Pt)>120]                                                                                                        " , "MASK=[001]" },
{ "name=[DiMuon]"              , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900] && [Muon12(Pt)>1296]                                                                              " , "MASK=[001]" },
{ "name=[B1gas]"               , "rate==[100]" , "conditions= [SumEt>208]          && [PU(Mult)<18]                                                                                  " , "MASK=[010]" },
{ "name=[B2gas]"               , "rate==[100]" , "conditions= [SumEt<135]          && [PU(Mult)>9]                                                                                   " , "MASK=[100]" },
{ "name=[CALO]"                , "rate==[100]" , "conditions= [Hadron(Et)>10]      && [Spd(Mult)>2]                                                                                  " , "MASK=[000]" },
{ "name=[HRCDiHadron,lowMult]" , "rate==[20]"  , "conditions= [HRC(B)<1]           && [HRC(F)<1]          && [Hadron(Et)>15]     && [SumEtPrev<200] && [Spd(Mult)<20] && [PU(Mult)<2]" , "MASK=[001]" },
{ "name=[Muon,lowMult]"        , "rate==[100]" , "conditions= [Spd(Mult)<20]       && [Muon1(Pt)>8]                                                                                  " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"      , "rate==[100]" , "conditions= [Spd(Mult)<20]       && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                                                           " , "MASK=[001]" },
{ "name=[Electron,lowMult]"    , "rate==[100]" , "conditions= [Electron(Et)>50]    && [Spd(Mult)<15]                                                                                 " , "MASK=[001]" },
{ "name=[Photon,lowMult]"      , "rate==[100]" , "conditions= [Photon(Et)>50]      && [Spd(Mult)<15]                                                                                 " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"        , "rate==[50]"  , "conditions= [Electron(Et)>20]    && [Photon(Et)>20]     && [Spd(Mult)<15]                                                          " , "MASK=[001]" }
};

//------------------------- TCK = 0x18A4 / Recipe name : PHYSICS_Sep2018_TCK18A4_0x18A4
ToolSvc.L0DUConfig.registerTCK += {"0x18A4"};
ToolSvc.L0DUConfig.TCK_0x18A4.Name = "PHYSICS_Sep2018_TCK18A4_0x18A4";
ToolSvc.L0DUConfig.TCK_0x18A4.Description ="TCK 0x18A4: Top of ramp TCK with tighter ElectronLoose and Herschel line prescaled to 0.8.";

ToolSvc.L0DUConfig.TCK_0x18A4.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x18A4.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x18A4.Conditions = {
{ "name=[Electron(Et)>20]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"                        },
{ "name=[Electron(Et)>37]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[37]"                        },
{ "name=[Electron(Et)>50]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"                        },
{ "name=[Electron(Et)>99]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[99]"                        },
{ "name=[Electron(Et)>254]"   , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"                       },
{ "name=[Photon(Et)>20]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"                        },
{ "name=[Photon(Et)>50]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"   , "reported=[FALSE]" },
{ "name=[Photon(Et)>123]"     , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[123]"                       },
{ "name=[Photon(Et)>254]"     , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"  , "reported=[FALSE]" },
{ "name=[HRC(B)<1]"           , "data=[GlobalPi0(Et)]"      , "comparator=[<]" , "threshold=[1]"                         },
{ "name=[HRC(F)<1]"           , "data=[LocalPi0(Et)]"       , "comparator=[<]" , "threshold=[1]"                         },
{ "name=[Hadron(Et)>10]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"                        },
{ "name=[Hadron(Et)>15]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[15]"                        },
{ "name=[Hadron(Et)>158]"     , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[158]"                       },
{ "name=[Hadron(Et)>254]"     , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"  , "reported=[FALSE]" },
{ "name=[SumEt>1458]"         , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"                      },
{ "name=[SumEt<135]"          , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[135]"                       },
{ "name=[SumEt>208]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"                       },
{ "name=[SumEtPrev<200]"      , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[200]"  , "bx=[-1]"          },
{ "name=[SumEtPrev<1000]"     , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[1000]" , "bx=[-1]"          },
{ "name=[Spd_Had(Mult)<450]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"                       },
{ "name=[Spd_DiMu(Mult)<900]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"                       },
{ "name=[Spd(Mult)<20]"       , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"                        },
{ "name=[Spd(Mult)<15]"       , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[15]"                        },
{ "name=[Spd(Mult)>2]"        , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[PU(Mult)<18]"        , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[18]"                        },
{ "name=[PU(Mult)<2]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"                         },
{ "name=[PU(Mult)>9]"         , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"                         },
{ "name=[Muon1(Pt)>2]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[Muon1(Pt)>8]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"                         },
{ "name=[Muon1(Pt)>18]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[18]"                        },
{ "name=[Muon1(Pt)>35]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[35]"                        },
{ "name=[Muon1(Pt)>120]"      , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"                       },
{ "name=[Muon2(Pt)>2]"        , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"                         },
{ "name=[Muon12(Pt)>1296]"    , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[1296]"                      }
};

ToolSvc.L0DUConfig.TCK_0x18A4.Channels ={
{ "name=[JetEl]"               , "rate==[100]" , "conditions= [Electron(Et)>254]   && [Hadron(Et)>254]    && [SumEt>1458]                                                            " , "MASK=[001]" },
{ "name=[JetPh]"               , "rate==[100]" , "conditions= [Photon(Et)>254]     && [Hadron(Et)>254]    && [SumEt>1458]                                                            " , "MASK=[001]" },
{ "name=[Electron]"            , "rate==[100]" , "conditions= [Electron(Et)>99]    && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[ElectronLoose]"       , "rate==[0.4]" , "conditions= [Electron(Et)>37]    && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[Photon]"              , "rate==[100]" , "conditions= [Photon(Et)>123]     && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[Hadron]"              , "rate==[100]" , "conditions= [Hadron(Et)>158]     && [SumEtPrev<1000]    && [Spd_Had(Mult)<450]                                                     " , "MASK=[001]" },
{ "name=[Muon]"                , "rate==[100]" , "conditions= [SumEtPrev<1000]     && [Spd_Had(Mult)<450] && [Muon1(Pt)>35]                                                          " , "MASK=[001]" },
{ "name=[MuonLoose]"           , "rate==[0.2]" , "conditions= [SumEtPrev<1000]     && [Spd_Had(Mult)<450] && [Muon1(Pt)>18]                                                          " , "MASK=[001]" },
{ "name=[MuonEW]"              , "rate==[100]" , "conditions= [Muon1(Pt)>120]                                                                                                        " , "MASK=[001]" },
{ "name=[DiMuon]"              , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900] && [Muon12(Pt)>1296]                                                                              " , "MASK=[001]" },
{ "name=[B1gas]"               , "rate==[100]" , "conditions= [SumEt>208]          && [PU(Mult)<18]                                                                                  " , "MASK=[010]" },
{ "name=[B2gas]"               , "rate==[100]" , "conditions= [SumEt<135]          && [PU(Mult)>9]                                                                                   " , "MASK=[100]" },
{ "name=[CALO]"                , "rate==[100]" , "conditions= [Hadron(Et)>10]      && [Spd(Mult)>2]                                                                                  " , "MASK=[000]" },
{ "name=[HRCDiHadron,lowMult]" , "rate==[80]"  , "conditions= [HRC(B)<1]           && [HRC(F)<1]          && [Hadron(Et)>15]     && [SumEtPrev<200] && [Spd(Mult)<20] && [PU(Mult)<2]" , "MASK=[001]" },
{ "name=[Muon,lowMult]"        , "rate==[100]" , "conditions= [Spd(Mult)<20]       && [Muon1(Pt)>8]                                                                                  " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"      , "rate==[100]" , "conditions= [Spd(Mult)<20]       && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                                                           " , "MASK=[001]" },
{ "name=[Electron,lowMult]"    , "rate==[100]" , "conditions= [Electron(Et)>50]    && [Spd(Mult)<15]                                                                                 " , "MASK=[001]" },
{ "name=[Photon,lowMult]"      , "rate==[100]" , "conditions= [Photon(Et)>50]      && [Spd(Mult)<15]                                                                                 " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"        , "rate==[50]"  , "conditions= [Electron(Et)>20]    && [Photon(Et)>20]     && [Spd(Mult)<15]                                                          " , "MASK=[001]" }
};
