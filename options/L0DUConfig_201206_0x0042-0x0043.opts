//--------------------------------------------------------
// Created by R. Lefevre/O.Deschamps for TCK team: 2012-06-18 14:48
//--------------------------------------------------------
//------------------------- TCK = 0x0042 / Recipe name : PHYSICS_April2012_TCK0042_0x0042
ToolSvc.L0DUConfig.registerTCK += {"0x0042"};
ToolSvc.L0DUConfig.TCK_0x0042.Name = "PHYSICS_June2012_TCK0042_0x0042";
ToolSvc.L0DUConfig.TCK_0x0042.Description ="Same as L0 TCK 0x003D with new lowMult and 32 conditions only";

ToolSvc.L0DUConfig.TCK_0x0042.MuonFOIx = {6,5,0,4,8};
ToolSvc.L0DUConfig.TCK_0x0042.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x0042.Conditions = {
{ "name=[Electron(Et)>24]"  , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[24]"   },
{ "name=[Electron(Et)>50]"  , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[50]"   },
{ "name=[Electron(Et)>136]" , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[136]"  },
{ "name=[Electron(Et)>210]" , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[210]"  },
{ "name=[Photon(Et)>24]"    , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[24]"   },
{ "name=[Photon(Et)>50]"    , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[50]"   },
{ "name=[Photon(Et)>136]"   , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[136]"  },
{ "name=[Photon(Et)>210]"   , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[210]"  },
{ "name=[Hadron(Et)>12]"    , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[12]"   },
{ "name=[Hadron(Et)>25]"    , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[25]"   },
{ "name=[Hadron(Et)>50]"    , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[50]"   },
{ "name=[Hadron(Et)>181]"   , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[181]"  },
{ "name=[SumEt<200]"        , "data=[Sum(Et)]"            , "comparator=[<]"  , "threshold=[200]"  },
{ "name=[SumEt>250]"        , "data=[Sum(Et)]"            , "comparator=[>]"  , "threshold=[250]"  },
{ "name=[SumEt>2500]"       , "data=[Sum(Et)]"            , "comparator=[>]"  , "threshold=[2500]" },
{ "name=[Spd(Mult)>2]"      , "data=[Spd(Mult)]"          , "comparator=[>]"  , "threshold=[2]"    },
{ "name=[Spd(Mult)<10]"     , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[10]"   },
{ "name=[Spd(Mult)<900]"    , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[900]"  },
{ "name=[SpdHad(Mult)<600]" , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[600]"  },
{ "name=[SpdMu(Mult)<600]"  , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[600]"  },
{ "name=[SpdEPh(Mult)<600]" , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[600]"  },
{ "name=[PUPeak1=0]"        , "data=[PUPeak1(Cont)]"      , "comparator=[==]" , "threshold=[0]"    },
{ "name=[PUPeak2=0]"        , "data=[PUPeak2(Cont)]"      , "comparator=[==]" , "threshold=[0]"    },
{ "name=[PU(Mult)<3]"       , "data=[PUHits(Mult)]"       , "comparator=[<]"  , "threshold=[3]"    },
{ "name=[PU(Mult)>9]"       , "data=[PUHits(Mult)]"       , "comparator=[>]"  , "threshold=[9]"    },
{ "name=[PU(Mult)<30]"      , "data=[PUHits(Mult)]"       , "comparator=[<]"  , "threshold=[30]"   },
{ "name=[Muon1(Pt)>2]"      , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[2]"    },
{ "name=[Muon1(Pt)>5]"      , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[5]"    },
{ "name=[Muon1(Pt)>6]"      , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[6]"    },
{ "name=[Muon1(Pt)>44]"     , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[44]"   },
{ "name=[Muon2(Pt)>2]"      , "data=[Muon2(Pt)]"          , "comparator=[>]"  , "threshold=[2]"    },
{ "name=[Muon12(Pt)>1600]"  , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]"  , "threshold=[1600]" }
};

ToolSvc.L0DUConfig.TCK_0x0042.Channels ={
{ "name=[CALO]"             , "rate==[0.0001]" , "conditions= [Hadron(Et)>12]    && [Spd(Mult)>2]"                        , "MASK=[001]" },
{ "name=[MUON,minbias]"     , "rate==[0.001]"  , "conditions= [Muon1(Pt)>6]"                                              , "MASK=[001]" },
{ "name=[NoPVFlag]"         , "rate==[100]"    , "conditions= [PUPeak1=0]        && [PUPeak2=0]"                          , "MASK=[000]" },
{ "name=[ElectronNoSPD]"    , "rate==[0.01]"   , "conditions= [Electron(Et)>136]"                                         , "MASK=[001]" },
{ "name=[PhotonNoSPD]"      , "rate==[0.01]"   , "conditions= [Photon(Et)>136]"                                           , "MASK=[001]" },
{ "name=[HadronNoSPD]"      , "rate==[0.01]"   , "conditions= [Hadron(Et)>181]"                                           , "MASK=[001]" },
{ "name=[MuonNoSPD]"        , "rate==[0.01]"   , "conditions= [Muon1(Pt)>44]"                                             , "MASK=[001]" },
{ "name=[DiMuonNoSPD]"      , "rate==[0.01]"   , "conditions= [Muon12(Pt)>1600]"                                          , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]"    , "conditions= [SpdEPh(Mult)<600] && [ElectronNoSPD]"                      , "MASK=[001]" },
{ "name=[ElectronHi]"       , "rate==[100]"    , "conditions= [Electron(Et)>210] && [SpdEPh(Mult)<600]"                   , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]"    , "conditions= [SpdEPh(Mult)<600] && [PhotonNoSPD]"                        , "MASK=[001]" },
{ "name=[PhotonHi]"         , "rate==[100]"    , "conditions= [Photon(Et)>210]   && [SpdEPh(Mult)<600]"                   , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]"    , "conditions= [SpdHad(Mult)<600] && [HadronNoSPD]"                        , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]"    , "conditions= [SpdMu(Mult)<600]  && [MuonNoSPD]"                          , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]"    , "conditions= [Spd(Mult)<900]    && [DiMuonNoSPD]"                        , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]"    , "conditions= [Spd(Mult)<10]     && [Muon1(Pt)>5]"                        , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]"    , "conditions= [Spd(Mult)<10]     && [Muon1(Pt)>2]      && [Muon2(Pt)>2]"  , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]"    , "conditions= [Electron(Et)>50]  && [Spd(Mult)<10]"                       , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]"    , "conditions= [Photon(Et)>50]    && [Spd(Mult)<10]"                       , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[100]"    , "conditions= [Electron(Et)>24]  && [Photon(Et)>24]    && [Spd(Mult)<10]" , "MASK=[001]" },
{ "name=[DiHadron,lowMult]" , "rate==[25]"     , "conditions= [Hadron(Et)>25] && [Spd(Mult)<10] && [PU(Mult)<3]"          , "MASK=[001]" },
{ "name=[HighSumETJet]"     , "rate==[0.01]"   , "conditions= [SumEt>2500]"                                               , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]"    , "conditions= [SumEt>250]        && [PU(Mult)<30]"                        , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]"    , "conditions= [SumEt<200]        && [PU(Mult)>9]"                         , "MASK=[100]" }
};





//------------------------- TCK = 0x0043 / Recipe name : PHYSICS_April2012_TCK0043_0x0043
ToolSvc.L0DUConfig.registerTCK += {"0x0043"};
ToolSvc.L0DUConfig.TCK_0x0043.Name = "PHYSICS_June2012_TCK0043_0x0043";
ToolSvc.L0DUConfig.TCK_0x0043.Description ="Same as L0 TCK 0x00XX with backup DiHadron,lowMult & 32 conditions only";

ToolSvc.L0DUConfig.TCK_0x0043.MuonFOIx = {6,5,0,4,8};
ToolSvc.L0DUConfig.TCK_0x0043.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x0043.Conditions = {
{ "name=[Electron(Et)>24]"  , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[24]"   },
{ "name=[Electron(Et)>50]"  , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[50]"   },
{ "name=[Electron(Et)>136]" , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[136]"  },
{ "name=[Electron(Et)>210]" , "data=[Electron(Et)]"       , "comparator=[>]"  , "threshold=[210]"  },
{ "name=[Photon(Et)>24]"    , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[24]"   },
{ "name=[Photon(Et)>50]"    , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[50]"   },
{ "name=[Photon(Et)>136]"   , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[136]"  },
{ "name=[Photon(Et)>210]"   , "data=[Photon(Et)]"         , "comparator=[>]"  , "threshold=[210]"  },
{ "name=[Hadron(Et)>12]"    , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[12]"   },
{ "name=[Hadron(Et)>25]"    , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[25]"   },
{ "name=[Hadron(Et)>50]"    , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[50]"   },
{ "name=[Hadron(Et)>181]"   , "data=[Hadron(Et)]"         , "comparator=[>]"  , "threshold=[181]"  },
{ "name=[SumEt<200]"        , "data=[Sum(Et)]"            , "comparator=[<]"  , "threshold=[200]"  },
{ "name=[SumEt>100]"        , "data=[Sum(Et)]"            , "comparator=[>]"  , "threshold=[100]"  },
{ "name=[SumEt>250]"        , "data=[Sum(Et)]"            , "comparator=[>]"  , "threshold=[250]"  },
{ "name=[SumEt>2500]"       , "data=[Sum(Et)]"            , "comparator=[>]"  , "threshold=[2500]" },
{ "name=[Spd(Mult)>2]"      , "data=[Spd(Mult)]"          , "comparator=[>]"  , "threshold=[2]"    },
{ "name=[Spd(Mult)<10]"     , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[10]"   },
{ "name=[Spd(Mult)<900]"    , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[900]"  },
{ "name=[SpdHad(Mult)<600]" , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[600]"  },
{ "name=[SpdMu(Mult)<600]"  , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[600]"  },
{ "name=[SpdEPh(Mult)<600]" , "data=[Spd(Mult)]"          , "comparator=[<]"  , "threshold=[600]"  },
{ "name=[PUPeak1=0]"        , "data=[PUPeak1(Cont)]"      , "comparator=[==]" , "threshold=[0]"    },
{ "name=[PUPeak2=0]"        , "data=[PUPeak2(Cont)]"      , "comparator=[==]" , "threshold=[0]"    },
{ "name=[PU(Mult)>9]"       , "data=[PUHits(Mult)]"       , "comparator=[>]"  , "threshold=[9]"    },
{ "name=[PU(Mult)<30]"      , "data=[PUHits(Mult)]"       , "comparator=[<]"  , "threshold=[30]"   },
{ "name=[Muon1(Pt)>2]"      , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[2]"    },
{ "name=[Muon1(Pt)>5]"      , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[5]"    },
{ "name=[Muon1(Pt)>6]"      , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[6]"    },
{ "name=[Muon1(Pt)>44]"     , "data=[Muon1(Pt)]"          , "comparator=[>]"  , "threshold=[44]"   },
{ "name=[Muon2(Pt)>2]"      , "data=[Muon2(Pt)]"          , "comparator=[>]"  , "threshold=[2]"    },
{ "name=[Muon12(Pt)>1600]"  , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]"  , "threshold=[1600]" }
};

ToolSvc.L0DUConfig.TCK_0x0043.Channels ={
{ "name=[CALO]"             , "rate==[0.0001]" , "conditions= [Hadron(Et)>12]    && [Spd(Mult)>2]"                        , "MASK=[001]" },
{ "name=[MUON,minbias]"     , "rate==[0.001]"  , "conditions= [Muon1(Pt)>6]"                                              , "MASK=[001]" },
{ "name=[NoPVFlag]"         , "rate==[100]"    , "conditions= [PUPeak1=0]        && [PUPeak2=0]"                          , "MASK=[000]" },
{ "name=[ElectronNoSPD]"    , "rate==[0.01]"   , "conditions= [Electron(Et)>136]"                                         , "MASK=[001]" },
{ "name=[PhotonNoSPD]"      , "rate==[0.01]"   , "conditions= [Photon(Et)>136]"                                           , "MASK=[001]" },
{ "name=[HadronNoSPD]"      , "rate==[0.01]"   , "conditions= [Hadron(Et)>181]"                                           , "MASK=[001]" },
{ "name=[MuonNoSPD]"        , "rate==[0.01]"   , "conditions= [Muon1(Pt)>44]"                                             , "MASK=[001]" },
{ "name=[DiMuonNoSPD]"      , "rate==[0.01]"   , "conditions= [Muon12(Pt)>1600]"                                          , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]"    , "conditions= [SpdEPh(Mult)<600] && [ElectronNoSPD]"                      , "MASK=[001]" },
{ "name=[ElectronHi]"       , "rate==[100]"    , "conditions= [Electron(Et)>210] && [SpdEPh(Mult)<600]"                   , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]"    , "conditions= [SpdEPh(Mult)<600] && [PhotonNoSPD]"                        , "MASK=[001]" },
{ "name=[PhotonHi]"         , "rate==[100]"    , "conditions= [Photon(Et)>210]   && [SpdEPh(Mult)<600]"                   , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]"    , "conditions= [SpdHad(Mult)<600] && [HadronNoSPD]"                        , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]"    , "conditions= [SpdMu(Mult)<600]  && [MuonNoSPD]"                          , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]"    , "conditions= [Spd(Mult)<900]    && [DiMuonNoSPD]"                        , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]"    , "conditions= [Spd(Mult)<10]     && [Muon1(Pt)>5]"                        , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]"    , "conditions= [Spd(Mult)<10]     && [Muon1(Pt)>2]      && [Muon2(Pt)>2]"  , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]"    , "conditions= [Electron(Et)>50]  && [Spd(Mult)<10]"                       , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]"    , "conditions= [Photon(Et)>50]    && [Spd(Mult)<10]"                       , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[100]"    , "conditions= [Electron(Et)>24]  && [Photon(Et)>24]    && [Spd(Mult)<10]" , "MASK=[001]" },
{ "name=[DiHadron,lowMult]" , "rate==[10]"     , "conditions= [Hadron(Et)>25] && [SumEt>100] && [Spd(Mult)<10]"           , "MASK=[001]" },
{ "name=[HighSumETJet]"     , "rate==[0.01]"   , "conditions= [SumEt>2500]"                                               , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]"    , "conditions= [SumEt>250]        && [PU(Mult)<30]"                        , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]"    , "conditions= [SumEt<200]        && [PU(Mult)>9]"                         , "MASK=[100]" }
};

