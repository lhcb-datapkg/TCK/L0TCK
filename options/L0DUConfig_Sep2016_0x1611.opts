//-------------------------------------------------------
// Created by PVSS / L0DU  : 2016-09-09 18:02:44
//-------------------------------------------------------

//------------------------- TCK = 0x1611 / Recipe name : PHYSICS_Sep2016_TCK1611_0x1611
ToolSvc.L0DUConfig.registerTCK += {"0x1611"};
ToolSvc.L0DUConfig.TCK_0x1611.Name = "PHYSICS_Sep2016_TCK1611_0x1611";
ToolSvc.L0DUConfig.TCK_0x1611.Description ="TCK 0X1611: TCK for 2036 colliding bx with reduced output rate to counter OT throttling, CEP DiHadron with sumEt,prev, removed obsolete channels for luminosity";

ToolSvc.L0DUConfig.TCK_0x1611.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x1611.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x1611.Conditions = {
{ "name=[Electron(Et)>20]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Electron(Et)>50]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Electron(Et)>109]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[109]"               },
{ "name=[Electron(Et)>254]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Photon(Et)>20]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Photon(Et)>50]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Photon(Et)>124]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[124]"               },
{ "name=[Photon(Et)>254]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Hadron(Et)>10]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"                },
{ "name=[Hadron(Et)>25]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[25]"                },
{ "name=[Hadron(Et)>162]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[162]"               },
{ "name=[Hadron(Et)>254]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[SumEt>1458]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"              },
{ "name=[SumEt<135]"           , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[135]"               },
{ "name=[SumEt>208]"           , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"               },
{ "name=[SumEtPrev<250]"       , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[250]"   , "bx=[-1]" },
{ "name=[Spd_Jet(Mult)<10000]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10000]"             },
{ "name=[Spd_Had(Mult)<450]"   , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"               },
{ "name=[Spd_DiMu(Mult)<900]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"               },
{ "name=[Spd(Mult)<20]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"                },
{ "name=[Spd(Mult)<10]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10]"                },
{ "name=[Spd(Mult)<15]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[15]"                },
{ "name=[Spd(Mult)>2]"         , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[PU(Mult)<18]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[18]"                },
{ "name=[PU(Mult)<2]"          , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"                 },
{ "name=[PU(Mult)>9]"          , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"                 },
{ "name=[Muon1(Pt)>2]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon1(Pt)>8]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"                 },
{ "name=[Muon1(Pt)>30]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[30]"                },
{ "name=[Muon1(Pt)>120]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"               },
{ "name=[Muon2(Pt)>2]"         , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon12(Pt)>784]"      , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[784]"               }
};

ToolSvc.L0DUConfig.TCK_0x1611.Channels ={
{ "name=[JetEl]"            , "rate==[100]" , "conditions= [Electron(Et)>254]    && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[JetPh]"            , "rate==[100]" , "conditions= [Photon(Et)>254]      && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]" , "conditions= [Electron(Et)>109]    && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]" , "conditions= [Photon(Et)>124]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]" , "conditions= [Hadron(Et)>162]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]" , "conditions= [Spd_Had(Mult)<450]   && [Muon1(Pt)>30]                                                " , "MASK=[001]" },
{ "name=[MuonEW]"           , "rate==[100]" , "conditions= [Spd_Jet(Mult)<10000] && [Muon1(Pt)>120]                                               " , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900]  && [Muon12(Pt)>784]                                              " , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]" , "conditions= [SumEt>208]           && [PU(Mult)<18]                                                 " , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]" , "conditions= [SumEt<135]           && [PU(Mult)>9]                                                  " , "MASK=[100]" },
{ "name=[CALO]"             , "rate==[100]" , "conditions= [Hadron(Et)>10]       && [Spd(Mult)>2]                                                 " , "MASK=[000]" },
{ "name=[DiHadron,lowMult]" , "rate==[100]" , "conditions= [Hadron(Et)>25]       && [SumEtPrev<250]     && [Spd(Mult)<10] && [PU(Mult)<2]         " , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>8]                                                 " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                          " , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]" , "conditions= [Electron(Et)>50]     && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]" , "conditions= [Photon(Et)>50]       && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[50]"  , "conditions= [Electron(Et)>20]     && [Photon(Et)>20]     && [Spd(Mult)<15]                         " , "MASK=[001]" }
};
