//-------------------------------------------------------
// Created by PVSS / L0DU  : 2016-06-04 19:44:06
//-------------------------------------------------------

//------------------------- TCK = 0x1609 / Recipe name : PHYSICS_June2016_TCK1609_0x1609
ToolSvc.L0DUConfig.registerTCK += {"0x1609"};
ToolSvc.L0DUConfig.TCK_0x1609.Name = "PHYSICS_June2016_TCK1609_0x1609";
ToolSvc.L0DUConfig.TCK_0x1609.Description ="TCK 0X1609: TCK for 1715 colliding bx, single and dimuon compromise, nominal CEP DiHadron, with sumEt,prev, removed obsolete lumi channels";

ToolSvc.L0DUConfig.TCK_0x1609.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x1609.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x1609.Conditions = {
{ "name=[Electron(Et)>20]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Electron(Et)>50]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Electron(Et)>98]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[98]"                },
{ "name=[Electron(Et)>254]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Photon(Et)>20]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Photon(Et)>50]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Photon(Et)>118]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[118]"               },
{ "name=[Photon(Et)>254]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Hadron(Et)>10]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"                },
{ "name=[Hadron(Et)>154]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[154]"               },
{ "name=[Hadron(Et)>254]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Hadron(Et)>25]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[25]"                },
{ "name=[SumEt>1458]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"              },
{ "name=[SumEt<135]"           , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[167]"               },
{ "name=[SumEt>208]"           , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"               },
{ "name=[SumEtPrev<250]"       , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[250]"   , "bx=[-1]" },
{ "name=[Spd_Jet(Mult)<10000]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10000]"             },
{ "name=[Spd_Had(Mult)<450]"   , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"               },
{ "name=[Spd_DiMu(Mult)<900]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"               },
{ "name=[Spd(Mult)<20]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"                },
{ "name=[Spd(Mult)<10]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10]"                },
{ "name=[Spd(Mult)<15]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[15]"                },
{ "name=[Spd(Mult)>2]"         , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[PU(Mult)<18]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[30]"                },
{ "name=[PU(Mult)<2]"          , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"                 },
{ "name=[PU(Mult)>9]"          , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"                 },
{ "name=[Muon1(Pt)>2]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon1(Pt)>8]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"                 },
{ "name=[Muon1(Pt)>26]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[26]"                },
{ "name=[Muon1(Pt)>120]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"               },
{ "name=[Muon2(Pt)>2]"         , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon12(Pt)>676]"      , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[676]"               }
};

ToolSvc.L0DUConfig.TCK_0x1609.Channels ={
{ "name=[JetEl]"            , "rate==[100]" , "conditions= [Electron(Et)>254]    && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[JetPh]"            , "rate==[100]" , "conditions= [Photon(Et)>254]      && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]" , "conditions= [Electron(Et)>98]     && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]" , "conditions= [Photon(Et)>118]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]" , "conditions= [Hadron(Et)>154]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]" , "conditions= [Spd_Had(Mult)<450]   && [Muon1(Pt)>26]                                                " , "MASK=[001]" },
{ "name=[MuonEW]"           , "rate==[100]" , "conditions= [Spd_Jet(Mult)<10000] && [Muon1(Pt)>120]                                               " , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900]  && [Muon12(Pt)>676]                                              " , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]" , "conditions= [SumEt>208]           && [PU(Mult)<18]                                                 " , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]" , "conditions= [SumEt<135]           && [PU(Mult)>9]                                                  " , "MASK=[100]" },
{ "name=[CALO]"             , "rate==[100]" , "conditions= [Hadron(Et)>10]       && [Spd(Mult)>2]                                                 " , "MASK=[000]" },
{ "name=[DiHadron,lowMult]" , "rate==[100]" , "conditions= [Hadron(Et)>25]       && [SumEtPrev<250]     && [Spd(Mult)<10] && [PU(Mult)<2]         " , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>8]                                                 " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                          " , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]" , "conditions= [Electron(Et)>50]     && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]" , "conditions= [Photon(Et)>50]       && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[50]"  , "conditions= [Electron(Et)>20]     && [Photon(Et)>20]     && [Spd(Mult)<15]                         " , "MASK=[001]" }
};

//------------------------- TCK = 0x160A / Recipe name : PHYSICS_June2016_TCK160A_0x160A
ToolSvc.L0DUConfig.registerTCK += {"0x160A"};
ToolSvc.L0DUConfig.TCK_0x160A.Name = "PHYSICS_June2016_TCK160A_0x160A";
ToolSvc.L0DUConfig.TCK_0x160A.Description ="TCK 0X160A: TCK for 1715 colliding bx, single and dimuon compromise, nominal CEP DiHadron, without sumEt,prev, removed obsolete channels for luminosity";

ToolSvc.L0DUConfig.TCK_0x160A.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x160A.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x160A.Conditions = {
{ "name=[Electron(Et)>20]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"    },
{ "name=[Electron(Et)>50]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"    },
{ "name=[Electron(Et)>98]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[98]"    },
{ "name=[Electron(Et)>254]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"   },
{ "name=[Photon(Et)>20]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"    },
{ "name=[Photon(Et)>50]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"    },
{ "name=[Photon(Et)>118]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[118]"   },
{ "name=[Photon(Et)>254]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"   },
{ "name=[Hadron(Et)>10]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"    },
{ "name=[Hadron(Et)>154]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[154]"   },
{ "name=[Hadron(Et)>254]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"   },
{ "name=[Hadron(Et)>17]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[17]"    },
{ "name=[SumEt>1458]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"  },
{ "name=[SumEt<135]"           , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[167]"   },
{ "name=[SumEt>208]"           , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"   },
{ "name=[Spd_Jet(Mult)<10000]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10000]" },
{ "name=[Spd_Had(Mult)<450]"   , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"   },
{ "name=[Spd_DiMu(Mult)<900]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"   },
{ "name=[Spd(Mult)<20]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"    },
{ "name=[Spd(Mult)>2]"         , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"     },
{ "name=[PU(Mult)<18]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[30]"    },
{ "name=[PU(Mult)<2]"          , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"     },
{ "name=[PU(Mult)>9]"          , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"     },
{ "name=[Muon1(Pt)>2]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"     },
{ "name=[Muon1(Pt)>8]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"     },
{ "name=[Muon1(Pt)>26]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[26]"    },
{ "name=[Muon1(Pt)>120]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"   },
{ "name=[Muon2(Pt)>2]"         , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"     },
{ "name=[Muon12(Pt)>676]"      , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[676]"   }
};

ToolSvc.L0DUConfig.TCK_0x160A.Channels ={
{ "name=[JetEl]"            , "rate==[100]" , "conditions= [Electron(Et)>254]    && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[JetPh]"            , "rate==[100]" , "conditions= [Photon(Et)>254]      && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]" , "conditions= [Electron(Et)>98]     && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]" , "conditions= [Photon(Et)>118]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]" , "conditions= [Hadron(Et)>154]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]" , "conditions= [Spd_Had(Mult)<450]   && [Muon1(Pt)>26]                                                " , "MASK=[001]" },
{ "name=[MuonEW]"           , "rate==[100]" , "conditions= [Spd_Jet(Mult)<10000] && [Muon1(Pt)>120]                                               " , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900]  && [Muon12(Pt)>676]                                              " , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]" , "conditions= [SumEt>208]           && [PU(Mult)<18]                                                 " , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]" , "conditions= [SumEt<135]           && [PU(Mult)>9]                                                  " , "MASK=[100]" },
{ "name=[CALO]"             , "rate==[100]" , "conditions= [Hadron(Et)>10]       && [Spd(Mult)>2]                                                 " , "MASK=[000]" },
{ "name=[DiHadron,lowMult]" , "rate==[3]"   , "conditions= [Hadron(Et)>17]       && [Spd(Mult)<20]      && [PU(Mult)<2]                           " , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>8]                                                 " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                          " , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]" , "conditions= [Electron(Et)>50]     && [Spd(Mult)<20]                                                " , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]" , "conditions= [Photon(Et)>50]       && [Spd(Mult)<20]                                                " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[50]"  , "conditions= [Electron(Et)>20]     && [Photon(Et)>20]     && [Spd(Mult)<20]                         " , "MASK=[001]" }
};

//------------------------- TCK = 0x160B / Recipe name : PHYSICS_May2016_TCK160B_0x160B
ToolSvc.L0DUConfig.registerTCK += {"0x160B"};
ToolSvc.L0DUConfig.TCK_0x160B.Name = "PHYSICS_May2016_TCK160B_0x160B";
ToolSvc.L0DUConfig.TCK_0x160B.Description ="TCK 0X160B: TCK for 2000 colliding bx, CEP DiHadron with sumEt,prev, removed obsolete channels for luminosity";

ToolSvc.L0DUConfig.TCK_0x160B.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x160B.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x160B.Conditions = {
{ "name=[Electron(Et)>20]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Electron(Et)>50]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Electron(Et)>108]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[108]"               },
{ "name=[Electron(Et)>254]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Photon(Et)>20]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Photon(Et)>50]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Photon(Et)>124]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[124]"               },
{ "name=[Photon(Et)>254]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Hadron(Et)>10]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"                },
{ "name=[Hadron(Et)>154]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[154]"               },
{ "name=[Hadron(Et)>254]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Hadron(Et)>25]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[25]"                },
{ "name=[SumEt>1458]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"              },
{ "name=[SumEt<135]"           , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[167]"               },
{ "name=[SumEt>208]"           , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"               },
{ "name=[SumEtPrev<250]"       , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[250]"   , "bx=[-1]" },
{ "name=[Spd_Jet(Mult)<10000]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10000]"             },
{ "name=[Spd_Had(Mult)<450]"   , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"               },
{ "name=[Spd_DiMu(Mult)<900]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"               },
{ "name=[Spd(Mult)<20]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"                },
{ "name=[Spd(Mult)<10]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10]"                },
{ "name=[Spd(Mult)<15]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[15]"                },
{ "name=[Spd(Mult)>2]"         , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[PU(Mult)<18]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[30]"                },
{ "name=[PU(Mult)<2]"          , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"                 },
{ "name=[PU(Mult)>9]"          , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"                 },
{ "name=[Muon1(Pt)>2]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon1(Pt)>8]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"                 },
{ "name=[Muon1(Pt)>30]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[30]"                },
{ "name=[Muon1(Pt)>120]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"               },
{ "name=[Muon2(Pt)>2]"         , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon12(Pt)>676]"      , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[676]"               }
};

ToolSvc.L0DUConfig.TCK_0x160B.Channels ={
{ "name=[JetEl]"            , "rate==[100]" , "conditions= [Electron(Et)>254]    && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[JetPh]"            , "rate==[100]" , "conditions= [Photon(Et)>254]      && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]" , "conditions= [Electron(Et)>108]    && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]" , "conditions= [Photon(Et)>124]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]" , "conditions= [Hadron(Et)>154]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]" , "conditions= [Spd_Had(Mult)<450]   && [Muon1(Pt)>30]                                                " , "MASK=[001]" },
{ "name=[MuonEW]"           , "rate==[100]" , "conditions= [Spd_Jet(Mult)<10000] && [Muon1(Pt)>120]                                               " , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900]  && [Muon12(Pt)>676]                                              " , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]" , "conditions= [SumEt>208]           && [PU(Mult)<18]                                                 " , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]" , "conditions= [SumEt<135]           && [PU(Mult)>9]                                                  " , "MASK=[100]" },
{ "name=[CALO]"             , "rate==[100]" , "conditions= [Hadron(Et)>10]       && [Spd(Mult)>2]                                                 " , "MASK=[000]" },
{ "name=[DiHadron,lowMult]" , "rate==[80]"  , "conditions= [Hadron(Et)>25]       && [SumEtPrev<250]     && [Spd(Mult)<10] && [PU(Mult)<2]         " , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>8]                                                 " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                          " , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]" , "conditions= [Electron(Et)>50]     && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]" , "conditions= [Photon(Et)>50]       && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[50]"  , "conditions= [Electron(Et)>20]     && [Photon(Et)>20]     && [Spd(Mult)<15]                         " , "MASK=[001]" }
};

//------------------------- TCK = 0x160C / Recipe name : PHYSICS_May2016_TCK160C_0x160C
ToolSvc.L0DUConfig.registerTCK += {"0x160C"};
ToolSvc.L0DUConfig.TCK_0x160C.Name = "PHYSICS_May2016_TCK160C_0x160C";
ToolSvc.L0DUConfig.TCK_0x160C.Description ="TCK 0X160C: TCK for 2442 colliding bx, CEP DiHadron with sumEt,prev, removed obsolete channels for luminosity";

ToolSvc.L0DUConfig.TCK_0x160C.MuonFOIx = {4,3,0,4,8};
ToolSvc.L0DUConfig.TCK_0x160C.MuonFOIy = {0,0,0,1,1};

ToolSvc.L0DUConfig.TCK_0x160C.Conditions = {
{ "name=[Electron(Et)>20]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Electron(Et)>50]"     , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Electron(Et)>110]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[110]"               },
{ "name=[Electron(Et)>254]"    , "data=[Electron(Et)]"       , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Photon(Et)>20]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[20]"                },
{ "name=[Photon(Et)>50]"       , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[50]"                },
{ "name=[Photon(Et)>126]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[126]"               },
{ "name=[Photon(Et)>254]"      , "data=[Photon(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[Hadron(Et)>10]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[10]"                },
{ "name=[Hadron(Et)>25]"       , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[25]"                },
{ "name=[Hadron(Et)>164]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[164]"               },
{ "name=[Hadron(Et)>254]"      , "data=[Hadron(Et)]"         , "comparator=[>]" , "threshold=[254]"               },
{ "name=[SumEt>1458]"          , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[1458]"              },
{ "name=[SumEt<135]"           , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[167]"               },
{ "name=[SumEt>208]"           , "data=[Sum(Et)]"            , "comparator=[>]" , "threshold=[208]"               },
{ "name=[SumEtPrev<250]"       , "data=[Sum(Et)]"            , "comparator=[<]" , "threshold=[250]"   , "bx=[-1]" },
{ "name=[Spd_Jet(Mult)<10000]" , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10000]"             },
{ "name=[Spd_Had(Mult)<450]"   , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[450]"               },
{ "name=[Spd_DiMu(Mult)<900]"  , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[900]"               },
{ "name=[Spd(Mult)<20]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[20]"                },
{ "name=[Spd(Mult)<10]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[10]"                },
{ "name=[Spd(Mult)<15]"        , "data=[Spd(Mult)]"          , "comparator=[<]" , "threshold=[15]"                },
{ "name=[Spd(Mult)>2]"         , "data=[Spd(Mult)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[PU(Mult)<18]"         , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[30]"                },
{ "name=[PU(Mult)<2]"          , "data=[PUHits(Mult)]"       , "comparator=[<]" , "threshold=[2]"                 },
{ "name=[PU(Mult)>9]"          , "data=[PUHits(Mult)]"       , "comparator=[>]" , "threshold=[9]"                 },
{ "name=[Muon1(Pt)>2]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon1(Pt)>8]"         , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[8]"                 },
{ "name=[Muon1(Pt)>36]"        , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[36]"                },
{ "name=[Muon1(Pt)>120]"       , "data=[Muon1(Pt)]"          , "comparator=[>]" , "threshold=[120]"               },
{ "name=[Muon2(Pt)>2]"         , "data=[Muon2(Pt)]"          , "comparator=[>]" , "threshold=[2]"                 },
{ "name=[Muon12(Pt)>900]"      , "data=[DiMuonProd(Pt1Pt2)]" , "comparator=[>]" , "threshold=[900]"               }
};

ToolSvc.L0DUConfig.TCK_0x160C.Channels ={
{ "name=[JetEl]"            , "rate==[100]" , "conditions= [Electron(Et)>254]    && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[JetPh]"            , "rate==[100]" , "conditions= [Photon(Et)>254]      && [Hadron(Et)>254]    && [SumEt>1458]   && [Spd_Jet(Mult)<10000]" , "MASK=[001]" },
{ "name=[Electron]"         , "rate==[100]" , "conditions= [Electron(Et)>110]    && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Photon]"           , "rate==[100]" , "conditions= [Photon(Et)>126]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Hadron]"           , "rate==[100]" , "conditions= [Hadron(Et)>164]      && [Spd_Had(Mult)<450]                                           " , "MASK=[001]" },
{ "name=[Muon]"             , "rate==[100]" , "conditions= [Spd_Had(Mult)<450]   && [Muon1(Pt)>36]                                                " , "MASK=[001]" },
{ "name=[MuonEW]"           , "rate==[100]" , "conditions= [Spd_Jet(Mult)<10000] && [Muon1(Pt)>120]                                               " , "MASK=[001]" },
{ "name=[DiMuon]"           , "rate==[100]" , "conditions= [Spd_DiMu(Mult)<900]  && [Muon12(Pt)>900]                                              " , "MASK=[001]" },
{ "name=[B1gas]"            , "rate==[100]" , "conditions= [SumEt>208]           && [PU(Mult)<18]                                                 " , "MASK=[010]" },
{ "name=[B2gas]"            , "rate==[100]" , "conditions= [SumEt<135]           && [PU(Mult)>9]                                                  " , "MASK=[100]" },
{ "name=[CALO]"             , "rate==[100]" , "conditions= [Hadron(Et)>10]       && [Spd(Mult)>2]                                                 " , "MASK=[000]" },
{ "name=[DiHadron,lowMult]" , "rate==[80]"  , "conditions= [Hadron(Et)>25]       && [SumEtPrev<250]     && [Spd(Mult)<10] && [PU(Mult)<2]         " , "MASK=[001]" },
{ "name=[Muon,lowMult]"     , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>8]                                                 " , "MASK=[001]" },
{ "name=[DiMuon,lowMult]"   , "rate==[100]" , "conditions= [Spd(Mult)<20]        && [Muon1(Pt)>2]       && [Muon2(Pt)>2]                          " , "MASK=[001]" },
{ "name=[Electron,lowMult]" , "rate==[100]" , "conditions= [Electron(Et)>50]     && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[Photon,lowMult]"   , "rate==[100]" , "conditions= [Photon(Et)>50]       && [Spd(Mult)<15]                                                " , "MASK=[001]" },
{ "name=[DiEM,lowMult]"     , "rate==[50]"  , "conditions= [Electron(Et)>20]     && [Photon(Et)>20]     && [Spd(Mult)<15]                         " , "MASK=[001]" }
};
